# Ansible Yara

An Ansible role to install and upgrade [Yara](https://virustotal.github.io/yara/) from source on Debian.
