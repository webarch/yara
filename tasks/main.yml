---
- name: Yara role skipped
  ansible.builtin.debug:
    msg: "The tasks in the Yara role are not being run since the yara variable is not true."
  when: >-
    ( yara is not defined ) or
    ( not yara | bool )
  tags:
    - yara

- name: Install or upgrade Yara
  block:

    - name: Include apt role local fact tasks
      ansible.builtin.include_role:
        name: apt
        tasks_from: local_facts.yml
      when: >-
        ( ansible_local.dpkg.arch is not defined ) or
        ( ansible_local.dpkg.installed is not defined ) or
        ( ansible_local.gpg.version is not defined ) or
        ( ansible_local.bash.path is not defined )

    - name: Check if Yara is installed
      ansible.builtin.command: which yara
      check_mode: false
      changed_when: false
      register: yara_which
      failed_when: yara_which.rc is not regex('0|1$')

    - name: Debug which yara results
      ansible.builtin.debug:
        var: yara_which
        verbosity: 2

    - name: Check the latest version of Yara
      ansible.builtin.uri:
        url: https://github.com/VirusTotal/yara/releases/latest
        method: HEAD
        status_code: 302
        follow_redirects: none
      check_mode: false
      register: yara_headers

    - name: Debug Yara URI
      ansible.builtin.debug:
        msg:
          - "Location: {{ yara_headers.location }}"
          - "Path: {{ yara_headers.location | urlsplit('path') }}"
          - "Version: {{ yara_headers.location | urlsplit('path') | basename | trim | regex_replace('^v') }}"
        verbosity: 1

    - name: Set a variable for the latest version available
      ansible.builtin.set_fact:
        yara_version: "{{ yara_headers.location | urlsplit('path') | basename | trim | regex_replace('^v') }}"

    - name: Check which version of Yara is installed
      block:

        - name: Check Yara version
          ansible.builtin.command: yara --version
          check_mode: false
          changed_when: false
          register: yara_version_installed

        - name: Set a fact for the installed version of Yara
          ansible.builtin.set_fact:
            yara_installed: "{{ yara_version_installed.stdout | trim }}"

        - name: Set a fact if a newer version is available
          ansible.builtin.set_fact:
            yara_upgrade: true
          when:
            - ( yara_installed is defined ) and ( yara_installed | length > 0 )
            - ( yara_version is defined ) and ( yara_version | length > 0 )
            - yara_installed != yara_version

      when: yara_which.rc == 0

    - name: Install Yara
      block:

        - name: Requirements present
          ansible.builtin.apt:
            pkg:
              - automake
              - libtool
              - libssl-dev
              - libjansson-dev
              - libmagic-dev
              - make
              - gcc
              - pkg-config
            state: present

        - name: "Download Yara {{ yara_version }}"
          ansible.builtin.get_url:
            url: "https://github.com/VirusTotal/yara/archive/v{{ yara_version }}.tar.gz"
            dest: "/usr/local/src/yara-{{ yara_version }}.tar.gz"
            force: true
            mode: "0466"
            owner: root
            group: root

        - name: "Unarchive Yara {{ yara_version }}"
          ansible.builtin.unarchive:
            src: "/usr/local/src/yara-{{ yara_version }}.tar.gz"
            dest: /usr/local/src
            remote_src: true

        - name: "Bootstrap Yara {{ yara_version }}"
          ansible.builtin.shell: ./bootstrap.sh
          args:
            chdir: "/usr/local/src/yara-{{ yara_version }}"
            executable: "{{ ansible_local.bash.path }}"
          changed_when: true

        - name: "Configure Yara {{ yara_version }}"
          ansible.builtin.shell: ./configure
          args:
            chdir: "/usr/local/src/yara-{{ yara_version }}"
            executable: "{{ ansible_local.bash.path }}"
          changed_when: true

        - name: "Make Yara {{ yara_version }}"
          community.general.make:
            chdir: "/usr/local/src/yara-{{ yara_version }}"

        - name: "Check Yara {{ yara_version }}"
          community.general.make:
            target: check
            chdir: "/usr/local/src/yara-{{ yara_version }}"

        - name: /usr/local/lib in /etc/ld.so.conf
          ansible.builtin.lineinfile:
            path: /etc/ld.so.conf
            line: /usr/local/lib
            state: present
            owner: root
            group: root
            mode: "0466"

        - name: Run ldconfig
          ansible.builtin.command: ldconfig
          changed_when: true

        - name: "Install Yara {{ yara_version }}"
          community.general.make:
            target: install
            chdir: "/usr/local/src/yara-{{ yara_version }}"

      when: ( yara_which.rc == 1 ) or ( yara_upgrade is defined and yara_upgrade )

  when:
    - yara is defined
    - yara | bool
  tags:
    - yara
...
